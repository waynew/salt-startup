set_hosts:
  file.managed:
    - name: /etc/hosts
    - contents_pillar: etc:hosts
    - template: jinja


update_hostname:
  file.managed:
    - name: /etc/hostname
    - contents_pillar: hostname

set_hostname:
  cmd.run:
    - name: /bin/hostname {{ pillar['hostname'] }}
    - onchanges:
      - file: update_hostname
      - file: set_hosts
