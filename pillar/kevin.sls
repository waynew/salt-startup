hostname: gru

etc:
  hosts: |
    # This file is managed by salt.
    # Changes will be obliterated with extreme prejudice
    127.0.0.1	localhost
    ::1		localhost ip6-localhost ip6-loopback
    ff02::1		ip6-allnodes
    ff02::2		ip6-allrouters
    {% raw -%}
    127.0.0.1	{{ pillar['hostname'] }}
    {% endraw %}
